from compte import *
import pytest


def test_creation():

    titulaire = Personne("John", "Doe")

    compte = Compte(1000, titulaire)

    assert compte.titulaire == titulaire

    assert compte.solde == 1000

def test_ajouter():

    titulaire = Personne("John", "Doe")

    compte = Compte(1000, titulaire)

    compte.crediter(500)

    assert compte.solde == 1500


def test_débiter():

    titulaire = Personne("John", "Doe")

    compte = Compte(1000, titulaire)

    compte.debiter(500)

    assert compte.solde == 500

def test_debiter_neg():
    titulaire = Personne("John", "Doe")

    compte = Compte(1000, titulaire)

    compte.debiter(1500)

    assert compte.solde < 0


test_ajouter()
test_creation()
test_débiter()