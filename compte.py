'''
Compte :
-solde : integer
-titulaire : Personne
------------------------------------
créditer(montant)
débiter(montant)

------------------------------------
initialiser(solde : int, titulaire : instance de Personne)
'''

class Personne :

    def __init__(self, nom, prenom) -> None:
        self.nom = nom
        self.prenom = prenom

    def __str__(self) -> str:
        return (f"{self.prenom} {self.nom}")

class Compte :
    def __init__(self, solde, titulaire:Personne) -> None:
        self.__solde = solde
        self.titulaire = titulaire

    @property
    def solde(self):
	    return self.__solde

    @solde.setter
    def solde(self, solde):

	    self.__solde = solde    

    def crediter(self, montant) :
        self.solde += montant

    def debiter(self, montant):
        self.solde -= montant

    def __str__(self) -> str:
        return (f"Compte de {self.titulaire} : solde = {self.solde}")


class Banque :

    def __init__(self, name) -> None:
        self.name = name
        self.comptes = []

    def ouvrir_compte(self, titulaire:Personne, depot) :

        nouveau_compte = Compte(depot,titulaire)

        self.comptes.append(nouveau_compte)

        return nouveau_compte


    def argent_total(self) :
        total = 0

        for compte in self.comptes :

            total += compte.solde
        
        return total


    def prelever_frais(self, frais) :

        for compte in self.comptes :
            compte.debiter(frais)


